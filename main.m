close all;
clear;
clc;
TRUE = 1;
FALSE = 0;
FREC_VS_TYME=1;
PLOT_ENERGY=0;
mode.visualization = PLOT_ENERGY;
mode.integrate = FALSE;
display('* Comienzo carga audio');
%songName = 'Los Tres - Un amor violento.mp3';
songName = 'Daft_Punk-Harder_Better_Faster.mp3';
%songName = 'A4-440.0.mp3';
%songName = '04 Carnaval.mp3';
%songName = 'metallica - one.mp3';
Periodo  = 0.2;%segundos
[y,Fs]   = audioread(['audio\',songName]);
y=y(:,1);
Length = Fs*Periodo;
slice = 1;
i = 1;
frec = zeros(uint64(length(y)/Length),1);
display('* Fin carga audio');
tiempo=linspace(1,length(y),length(frec));
tiempo=tiempo';
%% Se deben calibrar el rango de frecuencias a tomar en cuenta una vez calculada la potencia.   
NFFT = 2^nextpow2(Length);
frecuencia = Fs/2*linspace(0,1,NFFT/2+1);
fmax = 5000;
[~,indice]=max(-abs(frecuencia-fmax));
index_fmax=indice;
fmin = 1;
[~,indice]=max(-abs(frecuencia-fmin));
index_fmin=indice;
%% Se grafican los puntos de forma incremental.
%plot(0,0);
%hold on;
%pause(Periodo);
system(songName);
max_potencia=0.001;
crucesporcero=0;
if(mode.visualization==FREC_VS_TYME)
    hold on;
end
L=Length+1;
NFFT = 2^nextpow2(L);
while(i+Length<length(y))
    tic
    % se saca la frecuencia con maxima potencia de
    % un pedazo de y, que corresponde a x
    x = y(i:(i+Length));
    %soundsc(x,Fs);
    %L = length(x);
    %NFFT = 2^nextpow2(L);
    Y = fft(x,NFFT)/L;
    %frecuencia = Fs/2*linspace(0,1,NFFT/2+1);
    potencia = 2*abs(Y(1:NFFT/2+1));
    signal=smooth(potencia(index_fmin:index_fmax),30);
    [valor,indice]=max(signal);
    if(valor>max_potencia)
        max_potencia=valor;
    end
    if(mode.visualization==FREC_VS_TYME)
        frec(slice)=frecuencia(indice);
        %display(frec(slice));
        plot(tiempo(slice)/44100,frec(slice),'-o');
        slice=slice+1;
    else
        frec_signal=frecuencia(index_fmin:index_fmax);
        if(mode.integrate==TRUE)
            hold on;
        end
        plot(frec_signal,signal);
        hold on;
        [peak_values,peaks]=findpeaks(signal,'MINPEAKDISTANCE',20);
        if(mode.integrate==FALSE)
            plot(frec_signal(peaks),peak_values,'r');
        end        
        axis([fmin fmax 0 max_potencia]);
        hold off;
    end
    i=i+Length;
    if(Periodo-toc<0)
        crucesporcero=crucesporcero+1;
        display('warning: cruce por cero');
    end
    pause(Periodo-toc);
    %toc
end
