close all;
clear;
clc;
display('* Comienzo carga audio');
songName = 'A4-440.0.mp3';
%songName = 'Los Tres - Un amor violento.mp3';
Periodo  = 0.2;%segundos
[y,Fs]   = audioread(songName);
y=y(:,1);
Length = Fs*Periodo;
slice = 1;
i = 1;
frec = zeros(uint64(length(y)/Length),1);
display('* Fin carga audio');
tiempo=linspace(1,length(y),length(frec));
tiempo=tiempo';
%% Se grafican los puntos de forma incremental.
%plot(0,0);
hold on;
%pause(Periodo);
system(songName);
while(i+Length<length(y))
    tic
    % se saca la frecuencia con maxima potencia de
    % un pedazo de y, que corresponde a x
    x = y(i:(i+Length));
    L = length(x);
    NFFT = 2^nextpow2(L);
    Y = fft(x,NFFT)/L;
    frecuencia = Fs/2*linspace(0,1,NFFT/2+1);
    potencia = 2*abs(Y(1:NFFT/2+1));
    [valor,indice]=max(potencia);
    frec(slice)=frecuencia(indice);
    %display(frec(slice));
    plot(tiempo(slice)/44100,frec(slice),'-o');
    slice=slice+1;
    i=i+Length;
    pause(Periodo-toc);
end

%plot(tiempo/44100,frec);
